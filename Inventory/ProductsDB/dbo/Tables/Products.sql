﻿CREATE TABLE [dbo].[Products] (
    [ID]              INT             IDENTITY (1, 1) NOT NULL,
    [SKU]             VARCHAR (50)    NOT NULL,
    [Description]     VARCHAR (200)   NOT NULL,
    [QuantityInStock] INT             NOT NULL,
    [FinalPrice]      DECIMAL (18, 2) NOT NULL,
    [RegularPrice]    DECIMAL (18, 2) NOT NULL,
    [ApplyTaxes]      BIT             NOT NULL,
    [TaxRate]         DECIMAL (18, 2) NULL,
    [Image]           VARCHAR (100)   NULL,
    [Location]        VARCHAR (100)   NULL,
    CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED ([ID] ASC)
);

