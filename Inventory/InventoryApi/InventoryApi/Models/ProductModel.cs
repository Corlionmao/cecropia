﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace InventoryApi.Models
{
    public class ProductModel
    {

        public int ID { get; set; }
        [Required]
        [Display(Name = "Stock Keeping Unit")]
        public string SKU { get; set; }
        [Required]
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Required]
        [Display(Name = "Quantity In Stock")]
        public int QuantityInStock { get; set; }
        [Required]
        [Display(Name = "Final Price")]
        public decimal FinalPrice { get; set; }
        [Required]
        [Display(Name = "Regular Price")]
        public decimal RegularPrice { get; set; }
        [Required]
        [Display(Name = "Apply Taxes")]
        public bool ApplyTaxes { get; set; }
        [Required]
        [Display(Name = "Tax Rate")]
        public Nullable<decimal> TaxRate { get; set; }
        [Required]
        [Display(Name = "Image URL")]
        public string Image { get; set; }
        [Required]
        [Display(Name = "Location")]
        public string Location { get; set; }

    }
}