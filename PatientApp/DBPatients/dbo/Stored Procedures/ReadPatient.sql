﻿CREATE PROCEDURE [dbo].[ReadPatient]
	@Id int
AS 
BEGIN
	SELECT FirstName,LastName,Identification,BirthDate,
	Diseases, Nationality,PhoneNumber,BloodType
	FROM Patients
	 WHERE Id = @Id 
END