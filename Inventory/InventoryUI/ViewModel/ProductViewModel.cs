﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using InventoryUI.Models;

namespace InventoryUI.ViewModel
{
    public class ProductViewModel
    {
        public Product product { get; set; }
    }
}