﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PatientsApp.Models
{
    public class PatientModel
    {

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Identification { get; set; }
        public System.DateTime BirthDate { get; set; }
        public string Diseases { get; set; }
        public int Nationality { get; set; }
        public string PhoneNumber { get; set; }
        public Nullable<int> BloodType { get; set; }

        public virtual BloodType BloodType1 { get; set; }
        public virtual Country Country { get; set; }
    }
}