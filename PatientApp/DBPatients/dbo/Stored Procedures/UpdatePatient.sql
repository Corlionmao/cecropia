﻿CREATE PROCEDURE [dbo].[UpdatePatient]
	@Id int,
	@FirstName varchar(50),
	@LastName varchar(100),
	@Identification varchar(20),
	@BirthDate Date,
	@Diseases varchar(300),
	@Nationality int,
	@PhoneNumber varchar(20),
	@BloodType int
AS 
BEGIN
	UPDATE  Patients SET FirstName = @FirstName,LastName=@LastName,Identification=@Identification,
	BirthDate= @BirthDate,Diseases=@Diseases, Nationality=@Nationality,PhoneNumber=@PhoneNumber,
	 BloodType=@BloodType
	 WHERE ID = @Id 
END