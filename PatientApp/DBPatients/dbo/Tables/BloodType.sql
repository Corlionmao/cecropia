﻿CREATE TABLE [dbo].[BloodType] (
    [Id]   INT         IDENTITY (1, 1) NOT NULL,
    [Type] VARCHAR (3) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

