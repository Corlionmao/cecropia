﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using DataAccess;
using PatientsApp.Models;

namespace PatientsApp.Controllers
{
    public class PatientsController : ApiController
    {
        private PatientsDBEntities db = new PatientsDBEntities();
        
        SqlConnection con;

        [HttpGet]
        public IEnumerable<PatientModel> GetPatientList()
        {
            List<PatientModel> list = new List<PatientModel>();
            try
            {
                
                String connectionString = DBConexion.getConnection();
                
                con = new SqlConnection(connectionString);
                //Call to Sp ListPatient
                SqlCommand com = new SqlCommand("ListPatient", con);
                com.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter(com);
                DataTable dt = new DataTable();

                con.Open();

                da.Fill(dt);
                con.Close();

                //Linq get List
                list = (from DataRow dr in dt.Rows

                        select new PatientModel()
                        {
                            Id = Convert.ToInt32(dr["Id"]),
                            FirstName = dr["FirstName"].ToString(),
                            LastName = dr["LastName"].ToString(),
                            Identification = dr["Identification"].ToString(),
                            BirthDate = Convert.ToDateTime(dr["BirthDate"]),
                            Diseases = dr["Diseases"].ToString(),
                            Nationality = Convert.ToInt32(dr["nationality"]),
                            BloodType = Convert.ToInt32(dr["BloodType"]),
                            PhoneNumber = dr["PhoneNumber"].ToString(),
                        }
                            ).ToList();

            }catch(Exception ex)
            {
                throw ex;
            }

            return list;

                

        }

        [HttpPost]
        public IHttpActionResult PostPatient(Patient patient)
        {
            if (!ModelState.IsValid)
            {
                return  BadRequest(ModelState);
            }


            try
            {

                String connectionString = DBConexion.getConnection();

                using (SqlConnection dbConn = new SqlConnection(connectionString))
                {
                    dbConn.Open();


                    using (SqlTransaction dbTrans = dbConn.BeginTransaction())
                    {
                        try
                        {
                            
                            using (SqlCommand dbCommand = new SqlCommand("InsertPatient", dbConn))
                            {
                                dbCommand.CommandType = CommandType.StoredProcedure;
                                dbCommand.Transaction = dbTrans;


                                dbCommand.Parameters.AddWithValue("@FirstName", patient.FirstName);
                                dbCommand.Parameters.AddWithValue("@LastName", patient.LastName);
                                dbCommand.Parameters.AddWithValue("@Identification", patient.Identification);
                                dbCommand.Parameters.AddWithValue("@BirthDate", patient.BirthDate);
                                dbCommand.Parameters.AddWithValue("@Diseases", patient.Diseases);
                                dbCommand.Parameters.AddWithValue("@Nationality", patient.Nationality);
                                dbCommand.Parameters.AddWithValue("@PhoneNumber", patient.PhoneNumber);
                                dbCommand.Parameters.AddWithValue("@BloodType", patient.BloodType);
                                
                                dbCommand.ExecuteScalar();


                                dbTrans.Commit();
                            }

                        }
                        catch (SqlException sqlex)
                        {
                           dbTrans.Rollback();

                            throw sqlex;                         }
                    }

                    dbConn.Close();
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }


            return  CreatedAtRoute("DefaultApi", new { id = patient.Id }, patient);
        }

        [HttpPut]
        public IHttpActionResult PutPatient(Patient patient)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            try
            {

                String connectionString = DBConexion.getConnection();

                using (SqlConnection dbConn = new SqlConnection(connectionString))
                {
                    dbConn.Open();


                    using (SqlTransaction dbTrans = dbConn.BeginTransaction())
                    {
                        try
                        {

                            using (SqlCommand dbCommand = new SqlCommand("UpdatePatient", dbConn))
                            {
                                dbCommand.CommandType = CommandType.StoredProcedure;
                                dbCommand.Transaction = dbTrans;


                                dbCommand.Parameters.AddWithValue("@Id", patient.Id);
                                dbCommand.Parameters.AddWithValue("@FirstName", patient.FirstName);
                                dbCommand.Parameters.AddWithValue("@LastName", patient.LastName);
                                dbCommand.Parameters.AddWithValue("@Identification", patient.Identification);
                                dbCommand.Parameters.AddWithValue("@BirthDate", patient.BirthDate);
                                dbCommand.Parameters.AddWithValue("@Diseases", patient.Diseases);
                                dbCommand.Parameters.AddWithValue("@Nationality", patient.Nationality);
                                dbCommand.Parameters.AddWithValue("@PhoneNumber", patient.PhoneNumber);
                                dbCommand.Parameters.AddWithValue("@BloodType", patient.BloodType);
                                
                                dbCommand.ExecuteScalar();


                                dbTrans.Commit();
                            }

                        }
                        catch (SqlException sqlex)
                        {
                            dbTrans.Rollback();

                            throw sqlex;
                        }
                    }

                    dbConn.Close();
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }


            return CreatedAtRoute("DefaultApi", new { id = patient.Id }, patient);
        }
        
        [HttpDelete]
        public Boolean DeletePatient(int id)
        {
            try
            {

                String connectionString = DBConexion.getConnection();

                using (SqlConnection dbConn = new SqlConnection(connectionString))
                {
                    dbConn.Open();


                    using (SqlTransaction dbTrans = dbConn.BeginTransaction())
                    {
                        try
                        {

                            using (SqlCommand dbCommand = new SqlCommand("DeletePatient", dbConn))
                            {
                                dbCommand.CommandType = CommandType.StoredProcedure;
                                dbCommand.Transaction = dbTrans;


                                dbCommand.Parameters.AddWithValue("@Id", id);

                                
                                dbCommand.ExecuteNonQuery();


                                dbTrans.Commit();
                            }
                            return true;
                        }
                        catch (SqlException sqlex)
                        {
                            dbTrans.Rollback();

                            throw sqlex; // bubble up the exception and preserve the stack trace
                        }
                    }

                    dbConn.Close();
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Error : " + ex.Message);
            }


        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        
    }
}