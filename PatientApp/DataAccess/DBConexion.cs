﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class DBConexion
    {
        public static string getConnection()
        {
            //Sebre la escritura del web.config
            string cs = ConfigurationManager.ConnectionStrings["DbConnection"].ConnectionString;
            return cs;
        }
    }
}
