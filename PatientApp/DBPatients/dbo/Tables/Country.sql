﻿CREATE TABLE [dbo].[Country] (
    [Id]          INT          IDENTITY (1, 1) NOT NULL,
    [CountryName] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK__Country__3214EC07A120CF47] PRIMARY KEY CLUSTERED ([Id] ASC)
);

