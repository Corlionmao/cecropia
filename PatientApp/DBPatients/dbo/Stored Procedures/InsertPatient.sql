﻿
CREATE PROCEDURE [dbo].[InsertPatient]
	@FirstName varchar(50),
	@LastName varchar(100),
	@Identification varchar(20),
	@BirthDate Date,
	@Diseases varchar(300),
	@Nationality int,
	@PhoneNumber varchar(20),
	@BloodType int
AS
	INSERT INTO Patients(FirstName,LastName,Identification,BirthDate,Diseases, Nationality,
	PhoneNumber, BloodType) 
	VALUES (@FirstName,@LastName,@Identification,@BirthDate,@Diseases,@Nationality,@PhoneNumber,@BloodType)
	RETURN  SCOPE_IDENTITY()
