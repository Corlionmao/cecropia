﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace InventoryUI.Models
{
    public class Product
    {
        public int ID { get; set; }
        [Required]
        [Display(Name = "Stock Keeping Unit")]
        public string SKU { get; set; }
        [Required]
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Required]
        [Display(Name = "Quantity In Stock")]
        public int QuantityInStock { get; set; }
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        [Display(Name = "Final Price")]
        public decimal FinalPrice { get; set; }
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        [Display(Name = "Regular Price")]
        public decimal RegularPrice { get; set; }
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        [Display(Name = "Apply Taxes")]
        public bool ApplyTaxes { get; set; }
        [Required]
        [Display(Name = "Tax Rate")]
        public Nullable<decimal> TaxRate { get; set; }
        [Display(Name = "Image URL")]   
        public string Image { get; set; }
        [Required]
        [Display(Name = "Location")]
        public string Location { get; set; }

    }
}