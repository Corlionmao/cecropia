# Cecropia

Prueba Tecnica para Ingeniero ASP.Net Senior

Ingeniero: Marlon Gomez Velandia

Project Development:

-This is a very simple project and due to my short free time is not completed yet.
-Create a  WEB API 2 Restful project
-Project has a data acces layer
-Develop HTTP CRUD method calling stored procedures.
-Design the DB with a  SQL Express 2016 instance.
-After create the DB with Sql Management Studio, I did an import with VS SQL Server  project.
-Test the api with  POSTMAN app.
-Uploaded this Code with sourcetree app.

TODO:

-Logerror in database or windows log.
-Async methods
-UI ASP.NET 2.0 Layer to consume API
-Validations


Conclusion:

Please excuse me for the delay and small progress in this assignment, I have a really busy days.
I'll try to complete the assignment maybe for tomorrow night but I am not sure If I could finish it because every day comes with their own rush.

Thank you for your collaboration.

