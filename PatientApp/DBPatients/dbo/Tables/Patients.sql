﻿CREATE TABLE [dbo].[Patients] (
    [Id]             INT           IDENTITY (1, 1) NOT NULL,
    [FirstName]      VARCHAR (50)  NOT NULL,
    [LastName]       VARCHAR (100) NOT NULL,
    [Identification] VARCHAR (20)  NOT NULL,
    [BirthDate]      DATE          NOT NULL,
    [Diseases]       VARCHAR (300) NULL,
    [Nationality]    INT           NOT NULL,
    [PhoneNumber]    VARCHAR (20)  NOT NULL,
    [BloodType]      INT           NULL,
    CONSTRAINT [PK__Patients__3214EC07748B9797] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Patients_BloodType] FOREIGN KEY ([BloodType]) REFERENCES [dbo].[BloodType] ([Id]),
    CONSTRAINT [FK_Patients_Country] FOREIGN KEY ([Nationality]) REFERENCES [dbo].[Country] ([Id])
);

