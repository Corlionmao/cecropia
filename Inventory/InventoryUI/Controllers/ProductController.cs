﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InventoryUI.Models;
using InventoryUI.ViewModel;

namespace InventoryUI.Controllers
{
    public class ProductController : Controller
    {
        public ActionResult Index()
        {
            ProductClientApi pca = new ProductClientApi();
            ViewBag.listProducts = pca.findAll();

            return View();
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View("Create");
        }
        [HttpPost]
        public ActionResult Create(ProductViewModel cvm)
        {
            ProductClientApi pca = new ProductClientApi();
            pca.Create(cvm.product);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            ProductClientApi pca = new ProductClientApi();
            pca.Delete(id);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            try
            {
                ProductClientApi pca = new ProductClientApi();
                ProductViewModel pvm = new ProductViewModel();
                pvm.product = pca.find(id);
                return View("Edit", pvm);
            }catch(Exception ex)
            {
                 ModelState.AddModelError("Edit", "Product not found");
                    return View();
   
            }
        }
        [HttpPost]
        public ActionResult Edit(ProductViewModel pvm)
        {
            ProductClientApi pca = new ProductClientApi();
            pca.Edit(pvm.product);
            return RedirectToAction("Index");
        }
    }
}